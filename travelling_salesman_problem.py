import numpy as num
import matplotlib.pyplot as plt

final_path = []
all_paths = []

itr = 20

final_path.append(float('inf'))
final_path.append([0,1,2,3,4,0])

with open('input2.txt') as f:
    raw_data = f.read().splitlines()

data = [line.split() for line in raw_data]
del raw_data

size = len(data)
adjacency_matrix = num.array(data,dtype = 'float32')
del data

pheromen_matrix = num.zeros(shape = (size,size))
pheromen_matrix[:,:] = 0.059

for i in range(0,size):
    pheromen_matrix[i,i] = float('inf')

eta = 1/adjacency_matrix

for iterations in range(itr):
    path = []
    path.append(0)
    
    present_city = 0
    count = 0
    
    while count!=size-1:
        count += 1    
        probability_array1 = []
        probability_array2 = []
        for j in range(0,size):
            if j not in path:
                alpha = pheromen_matrix[present_city,j]
                beta = eta[present_city,j]
                gamma = alpha*(beta**2)
                probability_array1.append(j)
                probability_array2.append(gamma)
        next_city = probability_array1[probability_array2.index(max(probability_array2))]
        path.append(next_city)
        present_city = next_city
    
    path.append(0)
    length_of_path = 0
    
    for i in range(0,size):
        length_of_path += adjacency_matrix[path[i],path[i+1]]
        
    delta_change = 1/length_of_path
        
    for i in range(0,size):
        pheromen_matrix[path[i],path[i+1]] = 0.5*pheromen_matrix[path[i],path[i+1]] + delta_change

    all_paths.append(length_of_path)
    if final_path[0] >length_of_path:
        final_path[0] = length_of_path
        final_path[1] = path


print(final_path)
plt.scatter([x for x in range(itr)],all_paths)
plt.show()